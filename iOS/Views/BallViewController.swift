//
//  BallViewController.swift
//  LJ STAND
//
//  Created by Lachlan Grant on 10/5/18.
//  Copyright © 2018 Lachlan Grant. All rights reserved.
//

import UIKit
import MKConstants
import MKUtility
import MKUIKit

class BallViewController: UIViewController {
    internal var tappedButton: UIButton?

    @IBOutlet var ballStrengthLabel: UILabel!
    var ballView: CompassView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .clear
        ballView = CompassView(frame: calculateFrame())

        self.view.addSubview(ballView)

        self.generateConstraints(subView: ballView)

        if BluetoothController.shared.fakeData {
            
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        BluetoothController.shared.ballDelegate = self
        windowWasResized()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        BluetoothController.shared.ballDelegate = nil
    }

    func calculateFrame() -> CGRect {
        let windowFrame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y + 44.0, width: self.view.frame.width, height: self.view.frame.height - 44.0)
        let maxSize = windowFrame.size.width > windowFrame.size.height ? windowFrame.size.height * 0.9 : windowFrame.size.width * 0.9
        let returnFrame = CGRect(x: windowFrame.origin.x + windowFrame.size.width / 2 - maxSize / 2, y: windowFrame.origin.y + windowFrame.size.height / 2 - maxSize / 2, width: maxSize, height: maxSize)

        return returnFrame
    }

    func windowWasResized() {
        ballView.setNeedsDisplay()
    }
}

extension BallViewController: BluetoothControllerBallDelegate {
    func hasNewBallAngle(_ angle: Int) {
        self.ballView.rotate(Double(angle))
    }
    
    func hasNewBallStrength(_ str: Int) {
        self.ballStrengthLabel.text = "Ball Strength: \(str)"
    }
    
    
}

//extension CompassViewController: BluetoothControllerCompassDelegate {
//    func hasNewHeading(_ angle: Double, robot: RobotNumber) {
//        self.compass.rotate(angle)
//    }
//}
