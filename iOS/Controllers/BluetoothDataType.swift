//
//  BluetoothDataType.swift
//  LJ STAND
//
//  Created by Lachlan Grant on 6/4/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import Foundation

enum BluetoothDataType: Int {
    case noData
    case settings
    case info
    case ball
    case ballStr
    // case lightSensor
    // case compass
    // case raw
    // case btLinePosition
    // case btRobotPosition
    // case orbitAngle
    // case goal
}

enum RobotNumber: Int {
    case noRobot // No Robot Number was sent, just assume one robot set of data
    case one
    case two
}
