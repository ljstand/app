//
//  AppDelegate+Bluetooth.swift
//  LJ STAND
//
//  Created by Lachlan Grant on 24/4/17.
//  Copyright © 2017 Lachlan Grant. All rights reserved.
//

import UIKit
import MKUIKit
import MKUtility
import CoreBluetooth

extension AppDelegate: BluetoothMessageDelegate {
    func showInformation(_ message: String) {
        MKUIToast.shared.showMessage(message, state: .info, duration: 5.0)
        MKULog.shared.info(message)
    }
    
    func showError(_ message: String) {
        MKUIToast.shared.showMessage(message, state: .error, duration: 5.0)
        MKULog.shared.error(message)
    }
    
    func foundDevices(_ peripherals: [CBPeripheral]) {
        let alert = UIAlertController(title: "Connect to Device", message: nil, preferredStyle: .alert)
        
        for item in peripherals {
            alert.addAction(UIAlertAction(title: item.name, style: .default, handler: { (action) in
                BluetoothController.shared.connectTo(item)
            }))
        }
        
        window?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
}

